//
//  ViewController.swift
//  swiftStopWatch
//
//  Created by Click Labs on 1/16/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var count = 0
    
    var second = 0
    
    var minute = 0
    
    var timer = NSTimer()
    
    var playStop = false    // flag for play button
    
    @IBOutlet weak var minLabel: UILabel!
   
    @IBOutlet weak var millSecLabel: UILabel!
    
    @IBOutlet weak var secLabel: UILabel!
   
    @IBAction func stopButton(sender: AnyObject) {  // reset the stopwatch
       
        timer.invalidate()
        
        count = 0
        
        second = 0
        
        minute = 0
        
        minLabel.text = "00"
         secLabel.text = "00"
         millSecLabel.text = "00"
        playStop = false
        
    }
    
    @IBAction func playButton(sender: AnyObject) { // start the stopwatch
        
        if (playStop == false) {
        
            timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: Selector("clock"), userInfo: nil, repeats: true)
        
            playStop = true
   
        }
    }

    @IBAction func pauseButton(sender: AnyObject) {
        
        timer.invalidate()
       
        playStop = false
        
    }
    
    func clock() {  // changing the counter to sec and minute
        
        count++
        
        if count > 99 {  // update seconds
            
            count = 0
            
            second++
            
        }
        if second > 59  { // update minute
            second  = 0
            
            minute++
        }
        
        if minute == 59 && second == 59 && count == 99  {  // stop the clock  at 59:59:99
            
            timer.invalidate()
            
            playStop = false
        }
        
        if minute > 9
        {
        minLabel.text = " \(minute) "
        }
        else {
             minLabel.text = " 0\(minute) "
        }
        if second > 9
        {
            secLabel.text = " \(second) "
        }
        else {
            secLabel.text = " 0\(second) "
        }
        
        millSecLabel.text = " \(count) "
        
      // timerLabel.text = " \(minute): \( second ) : \(count) "
        
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

